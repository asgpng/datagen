# Overview

`data_gen` is a simple CLI tool for flexible and efficient generation of fake data in CSV output.

# Format

Desired characteristics of the data are specified in a [TOML](https://toml.io) file.

## Single file

The most basic form is a single output file:

```toml
rows = 1000000
output_file = "tmp.csv"
columns = [
    { name = "test_boolean", expr = "Boolean(0.2)" },
    { name = "test_float", expr = "Float(0.0..10.0)" },
    # ...
]
```

The following top-level keys are supported:

* `rows`: the number of rows to generate
* `output_file`: the destination of the generated file
* `columns`: array of column names and expressions. Column names must be unique.
* `key_column_name`: If specified, indicates that the values generated for the column should be
    unique. Note that this currently reduces execution to a single thread to ensure uniqueness.
* `missing_reference_behavior`: If specified, indicates desired behavior if the value of a reference
    column is not present in the weights file. Valid options are `FillWithNull`, `FillWithEmpty`,
    and `Panic`. The default if not specified is `Panic`.
* `sort_column_names`: If present, specifies that the output data should be sorted by the columns
    specified.  Note that this is an in-memory sort that happens after the data is generated, so it
    requires that the host machine has enough memory to fit the entire dataset in memory.
* `has_header`: If present, specifies whether to print column names in the CSV header. Defaults to
    `false`.
* `column_delimiter`: If present, overrides the default column delimiter of the tab character.

## Multiple files

In addition, a top-level `files` array supports generating multiple output files at once:

```toml
[[files]]
rows = 1000000
output_file = "tmp1.csv"
columns = [
    { name = "test_boolean", expr = "Boolean(0.2)" },
    { name = "test_float", expr = "Float(0.0..10.0)" },
    # ...
]

[[files]]
rows = 100
output_file = "tmp2.csv"
columns = [
    { name = "test_integer", expr = "Integer(20..40)" },
    { name = "test_set", expr = "Set(weights_1.csv, 0, 1)" },
    # ...
]
```

## Templates

In addition, a `template_file` may be specified. The template file must represent instructions for a
single file. The behavior in this case is for all parameters other than `output_file` to be
optional. If provided, a parameter will override the default from the template_file.

An exception to this is that columns from the template file will be included by default unless
overridden. Any extra columns not present in the template file will be appended to the end.

```toml
# File 1, tmp1.toml
rows = 1000000
output_file = "tmp1.csv"
columns = [
    { name = "test_boolean", expr = "Boolean(0.2)" },
    { name = "test_float", expr = "Float(0.0..10.0)" },
    # ...
]

# File 2 (will have 3 columns)
template_file = "tmp1.toml"
output_file = "tmp2.csv"
rows = 100
columns = [
    { name = "test_float", expr = "Float(0.0..100000.0)" },
    { name = "test_set", expr = "Set(weights_1.csv, 0, 1)" },
    # ...
]
```

## Column types

The following column types are supported:
* `Boolean`: a true/false value with specified probability
* `Float`: an `f64` within the specified range
* `Integer`: an `i64` within the specified range
* `Datetime`: a `NaiveDateTime` within the specified range, formatted as `%Y-%m-%d %H:%M:%S`
* `Date`: a `NaiveDate` within the specified range, formatted as `%Y-%m-%d`
* `Time`: a `Time` within the specified range, formatted as `%H:%M:%S`
* `Set`: a `String` which takes values sourced from a CSV file, optionally with specified weights.
    Additionally, if reference columns are specified, the values are determined by the values taken
    by a reference columns within the same row. (For example, the reference column might be "state"
    and the target column might be "city").
* `ContinuousDistribution`: an `f64` based on the distribution specified in the provided CSV file.
    The CSV file should include tuples of `(min, max, weight)`. The step of the distribution will be
    selected according to `weight`, and the specific value will be generated uniformly within the
    step.
* `IntegerExpression`: an `i64` generator which is composed of other generators:
    * Constants, e.g., `23`,
    * Column references, e.g., `Reference(other_column)`,
    * Random expressions, e.g., `Integer(0..2)`,
    * Binary expressions, e.g., `23 + Reference(other_column)`.
    **Note:** It is not supported to use this with any other `missing_reference_behavior` than
    `Panic`. Also, type-checking is currently lacking.
* `FloatExpression`: an `f64` generator which is composed of other generators:
    * Constants, e.g., `23.0`,
    * Column references, e.g., `Reference(other_column)`,
    * Random expressions, e.g., `Float(0.0..2.0)`,
    * Binary expressions, e.g., `23.0 + Reference(other_column)`.
    **Note:** It is not supported to use this with any other `missing_reference_behavior` than
    `Panic`. Also, type-checking is currently lacking.

See [examples](./examples) for specific examples.

# Commands

## `check`

Verify that the `layout_file` is able to be parsed correctly.

## `output`

Perform the work of generating data according to the characteristics specified in the input file.

# Performance

`data_gen` is fairly fast, because:
* It creates closures for generating each column value and calls them successively on each row,
    eliminating some redundant work in the inner loop.
* It leverages all available cores by default.
* It uses message-passing to write output to a single file (rather than writing to separate files
    and stitching them together at the end).

On an 8-core, 16-thread Ryzen 2700 with NVMe, it generates ~5 GB of files in about 16 seconds.

Additional performance could likely obtained in the future through:
* Combination of closures or use of `impl trait` to reduce cache lookups and avoid allocating
    closures on the heap.
* Support for concurrency in the context of data with keys (unique columns)
