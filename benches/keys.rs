use criterion::{criterion_group, criterion_main, Criterion, Throughput};
use data_gen::config::{ColumnOptions, MissingReferenceBehavior, TypeInfo, VerboseConfig};
use data_gen::process;

fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("keys");

    let configs = [
        get_key_config(100_000, 0, 999_999),
        get_key_config(1_000_000, 0, 10_000_000),
    ];

    for config in configs.iter() {
        group.throughput(Throughput::Elements(config.rows as u64));
        group.bench_with_input(format!("{} rows", config.rows), &configs, |b, _| {
            b.iter(|| {
                process(&config).expect("Failed to generate data");
            });
        });
    }

    group.finish();
}

fn get_key_config(rows: usize, min: i64, max: i64) -> VerboseConfig {
    VerboseConfig {
        rows,
        output_file: "/dev/null".to_owned(),
        has_header: true,
        column_delimiter: b',',
        column_options: ColumnOptions {
            columns: vec![("key".to_owned(), TypeInfo::Integer { min, max })],
            key_column_names: vec!["key".to_owned()],
            sort_column_names: vec![],
        },
        missing_reference_behavior: MissingReferenceBehavior::Panic,
    }
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
