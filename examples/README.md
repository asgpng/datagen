# How to run examples

```
cargo build --release
cd examples

../target/release/data_gen output --layout_file ./full.toml
../target/release/data_gen output --layout_file ./unique.toml
../target/release/data_gen output --layout_file ./multiple.toml
../target/release/data_gen output --layout_file ./using_template.toml
```
