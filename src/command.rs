use anyhow::{Context, Result};
use clap::ArgMatches;
use data_gen::config::{NestedVerboseConfig, VerboseConfig};
use data_gen::process;
use std::path::Path;

pub fn handle_layout_check_command(matches: &ArgMatches) -> Result<NestedVerboseConfig> {
    let file = matches.value_of("layout_file").unwrap();
    let file = Path::new(file);

    NestedVerboseConfig::from_path(file)
}

pub fn handle_output_command(matches: &ArgMatches) -> Result<()> {
    let commands = parse_output_command(matches)?;

    for c in commands {
        process(&c)?;
    }

    Ok(())
}

fn parse_output_command(matches: &ArgMatches) -> Result<Vec<VerboseConfig>> {
    let file = matches.value_of("layout_file").unwrap();
    let file = Path::new(file);

    let config = NestedVerboseConfig::from_path(file)
        .with_context(|| format!("Failed to parse file {:?}", file))?;
    Ok(config.files)
}
