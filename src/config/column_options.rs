use crate::config::TypeInfo;
use anyhow::{anyhow, Result};
use std::collections::HashSet;

/// Represents the characteristics of columns in an output.
///
/// This includes:
/// * Name and type
/// * List of key columns
/// * List of sort columns
#[derive(Debug)]
pub struct ColumnOptions {
    pub columns: Vec<(String, TypeInfo)>,

    /// Specifies the names of columns whose values should collectively be unique
    pub key_column_names: Vec<String>,

    /// Specifies that the output data should be sorted by the columns specified.
    /// Note that this is an in-memory sort that happens after the data is generated, so it requires
    /// that the host machine has enough memory to fit the entire dataset in memory.
    pub sort_column_names: Vec<String>,
}

impl ColumnOptions {
    pub fn new(
        columns: Vec<(String, TypeInfo)>,
        key_column_names: Vec<String>,
        sort_column_names: Vec<String>,
    ) -> Result<Self> {
        let column_names: HashSet<&String> = columns.iter().map(|(name, _)| name).collect();

        let extra_key_columns: Vec<String> = key_column_names
            .iter()
            .filter(|n| !column_names.contains(n))
            .map(|n| n.to_owned())
            .collect();
        if !extra_key_columns.is_empty() {
            return Err(anyhow!(
                "Invalid key column(s): {}",
                extra_key_columns.join(",")
            ));
        }

        let extra_sort_columns: Vec<String> = sort_column_names
            .iter()
            .filter(|n| !column_names.contains(n))
            .map(|n| n.to_owned())
            .collect();
        if !extra_sort_columns.is_empty() {
            return Err(anyhow!(
                "Invalid sort column(s): {}",
                extra_sort_columns.join(",")
            ));
        }

        let mut seen_column_names = HashSet::new();
        let mut duplicate_column_names = HashSet::new();
        for (name, _) in columns.iter() {
            if seen_column_names.contains(name) {
                duplicate_column_names.insert(name);
            }
            seen_column_names.insert(name);
        }

        if !duplicate_column_names.is_empty() {
            let names: Vec<String> = duplicate_column_names
                .into_iter()
                .map(|c| c.to_owned())
                .collect();
            return Err(anyhow!("Duplicate column(s): {}", names.join(",")));
        }

        Ok(ColumnOptions {
            columns,
            key_column_names,
            sort_column_names,
        })
    }
}
