use serde::{Deserialize, Serialize};

/// The behavior to assume if a generated value is not present among the reference set
#[derive(Deserialize, Serialize, Debug, PartialEq, Clone)]
pub enum MissingReferenceBehavior {
    /// Fill missing references with the string literal `NULL`
    FillWithNull,

    /// Fill missing references with the empty string literal
    FillWithEmpty,

    /// Panic when a missing reference is encountered
    Panic,
}
