#![allow(clippy::all)]

mod column;
mod column_options;
mod concise;
mod missing_reference_behavior;
mod nested_verbose;
mod parseable;
mod template;
mod type_info;
mod verbose;

// Generate parsing module
lalrpop_mod!(pub parser, "/config/parser.rs");

pub use column::{Column, ColumnExpression};
pub use column_options::ColumnOptions;
pub use concise::ConciseConfig;
pub use missing_reference_behavior::MissingReferenceBehavior;
pub use nested_verbose::NestedVerboseConfig;
pub use parseable::ParseableConfig;
pub use template::TemplateConfig;
pub use type_info::{F64Expression, I64Expression, Opcode, TypeInfo};
pub use verbose::VerboseConfig;

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::read_dir;
    use std::path::Path;

    #[test]
    fn all_example_configs_can_be_parsed() {
        let examples_directory_name = String::from("examples");
        let examples_path = Path::new(&examples_directory_name);
        let dir = read_dir(examples_path).unwrap();
        for entry in dir {
            let entry = entry.unwrap();
            let path_buf = entry.path();
            let path = path_buf.as_path();
            println!("{:?}", path);
            if let (Some(ext), Some(stem)) = (path.extension(), path.file_stem()) {
                if ext == "toml" && stem != "using_template" {
                    let result = NestedVerboseConfig::from_path(path);
                    assert!(
                        result.is_ok(),
                        "Failed to parse {:?}. Error: {:?}",
                        path,
                        result.err()
                    );
                }
            }
        }
    }

    #[test]
    fn duplicate_column_names_prohibited() {
        let columns = vec![
            ("hello".to_owned(), TypeInfo::Boolean { probability: 0.0 }),
            ("hello".to_owned(), TypeInfo::Boolean { probability: 0.0 }),
        ];

        let result = ColumnOptions::new(columns, vec![], vec![]);
        assert!(result.is_err());
        assert_eq!(
            format!("{}", result.err().unwrap()),
            "Duplicate column(s): hello"
        );
    }

    #[test]
    fn extra_key_columns_prohibited() {
        let columns = vec![("hello".to_owned(), TypeInfo::Boolean { probability: 0.0 })];

        let result =
            ColumnOptions::new(columns, vec!["world".to_owned(), "the".to_owned()], vec![]);
        assert!(result.is_err());
        assert_eq!(
            format!("{}", result.err().unwrap()),
            "Invalid key column(s): world,the"
        );
    }

    #[test]
    fn extra_sort_columns_prohibited() {
        let columns = vec![("hello".to_owned(), TypeInfo::Boolean { probability: 0.0 })];

        let result =
            ColumnOptions::new(columns, vec![], vec!["world".to_owned(), "the".to_owned()]);
        assert!(result.is_err());
        assert_eq!(
            format!("{}", result.err().unwrap()),
            "Invalid sort column(s): world,the"
        );
    }
}
