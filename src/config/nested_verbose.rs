use crate::config::{ConciseConfig, ParseableConfig, TemplateConfig, VerboseConfig};
use anyhow::{anyhow, Context, Result};
use std::convert::TryFrom;
use std::fs::File;
use std::io::Read;
use std::path::Path;

/// A configuration variant composed of multiple [`VerboseConfig`](VerboseConfig)s.
#[derive(Debug)]
pub struct NestedVerboseConfig {
    pub files: Vec<VerboseConfig>,
}

impl NestedVerboseConfig {
    fn from_concise(config: ConciseConfig) -> Result<NestedVerboseConfig> {
        Ok(NestedVerboseConfig {
            files: vec![VerboseConfig::try_from(config)?],
        })
    }

    fn from_template(config: TemplateConfig) -> Result<NestedVerboseConfig> {
        Ok(NestedVerboseConfig {
            files: vec![VerboseConfig::try_from(config)?],
        })
    }

    fn from_multiple(configs: Vec<ParseableConfig>) -> Result<NestedVerboseConfig> {
        let configs: Result<Vec<VerboseConfig>> = configs
            .into_iter()
            .map(|c| match c {
                ParseableConfig::Concise(spec) => Ok(VerboseConfig::try_from(spec)?),
                ParseableConfig::Template(template) => Ok(VerboseConfig::try_from(template)?),
                ParseableConfig::Multiple { .. } => {
                    Err(anyhow!("Multiple configs cannot be nested."))
                }
            })
            .collect();

        Ok(NestedVerboseConfig { files: configs? })
    }

    pub fn from_path(path: &Path) -> Result<NestedVerboseConfig> {
        let mut file =
            File::open(&path).with_context(|| format!("Could not read file at {:?}", path))?;
        let mut text = vec![];
        file.read_to_end(&mut text)
            .with_context(|| format!("Could not read file at {:?}", path))?;

        let text = String::from_utf8(text)?;

        let parsed: std::result::Result<ParseableConfig, toml::de::Error> = toml::from_str(&text);

        match parsed? {
            ParseableConfig::Template(template) => NestedVerboseConfig::from_template(template),
            ParseableConfig::Concise(spec) => NestedVerboseConfig::from_concise(spec),
            ParseableConfig::Multiple { files: specs } => NestedVerboseConfig::from_multiple(specs),
        }
    }
}
