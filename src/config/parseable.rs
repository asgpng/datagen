use crate::config::{ConciseConfig, TemplateConfig};
use serde::{Deserialize, Serialize};

/// Represents the set of possible configuration variants supported from input files
#[derive(Deserialize, Serialize, Debug)]
#[serde(untagged)]
pub enum ParseableConfig {
    Template(TemplateConfig),
    Concise(ConciseConfig),
    Multiple { files: Vec<ParseableConfig> },
}
