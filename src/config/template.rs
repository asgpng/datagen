use crate::config::{ColumnExpression, MissingReferenceBehavior};
use serde::{Deserialize, Serialize};

/// A configuration variant in which options are sourced first from a provided template file, with
/// overrides applied if provided.
#[derive(Deserialize, Serialize, Debug)]
pub struct TemplateConfig {
    // These are the only required properties. The rest, if provided, will override the values from
    // the template file.
    pub template_file: String,
    pub output_file: String,

    pub rows: Option<usize>,
    pub columns: Option<Vec<ColumnExpression>>,

    // If present, specifies the name of a column whose values should be unique
    pub key_column_name: Option<String>,

    // If present, specifies what should be done if a weight key is not present (default is panic)
    pub missing_reference_behavior: Option<MissingReferenceBehavior>,

    // If present, specifies that the output data should be sorted by the columns specified.
    // Note that this is an in-memory sort that happens after the data is generated, so it requires
    // that the host machine has enough memory to fit the entire dataset in memory.
    pub sort_column_names: Option<Vec<String>>,

    // Whether or not to write the column names as the first line of output. Defaults to false if
    // not provided.
    pub has_header: Option<bool>,

    // If provided, overrides the default column delimiter. If absent, the default is the tab
    // character.
    pub column_delimiter: Option<String>,
}
