use crate::config::{
    ColumnOptions, ConciseConfig, MissingReferenceBehavior, NestedVerboseConfig, TemplateConfig,
    TypeInfo,
};
use crate::constants::DEFAULT_COLUMN_DELIMITER;
use anyhow::{anyhow, Result};
use std::collections::{HashMap, HashSet};
use std::convert::TryFrom;
use std::path::Path;

/// A configuration variant in which all options are specified.
#[derive(Debug)]
pub struct VerboseConfig {
    pub rows: usize,
    pub output_file: String,
    pub has_header: bool,
    pub column_delimiter: u8,
    pub column_options: ColumnOptions,

    /// Specifies what should be done if a weight key is not present (default is panic)
    pub missing_reference_behavior: MissingReferenceBehavior,
}

impl TryFrom<ConciseConfig> for VerboseConfig {
    type Error = anyhow::Error;

    fn try_from(config: ConciseConfig) -> Result<VerboseConfig> {
        let types: Result<Vec<TypeInfo>> = config
            .columns
            .iter()
            .map(|c| TypeInfo::new(&c.expr))
            .collect();
        let types: Vec<TypeInfo> = types?;

        let missing_reference_behavior = config
            .missing_reference_behavior
            .unwrap_or(MissingReferenceBehavior::Panic);

        let columns: Vec<(String, TypeInfo)> = config
            .columns
            .iter()
            .map(|c| c.name.to_owned())
            .zip(types.into_iter())
            .collect();

        let key_column_names = if let Some(k) = config.key_column_name {
            vec![k]
        } else {
            vec![]
        };

        let include_header = config.has_header.unwrap_or(false);

        let column_delimiter: u8 = if let Some(cd) = config.column_delimiter {
            let bytes = cd.as_bytes();
            if bytes.len() > 1 {
                return Err(anyhow!(
                    "Could not parse column delimiter {} as an ASCII character.",
                    cd
                ));
            }
            bytes[0]
        } else {
            DEFAULT_COLUMN_DELIMITER
        };

        let sort_column_names = config.sort_column_names.unwrap_or_default();
        let column_options = ColumnOptions::new(columns, key_column_names, sort_column_names)?;
        Ok(VerboseConfig {
            rows: config.rows,
            output_file: config.output_file,
            column_options,
            missing_reference_behavior,
            has_header: include_header,
            column_delimiter,
        })
    }
}

impl TryFrom<TemplateConfig> for VerboseConfig {
    type Error = anyhow::Error;
    fn try_from(current_config: TemplateConfig) -> Result<VerboseConfig> {
        let template_path = Path::new(&current_config.template_file);
        let template_config = NestedVerboseConfig::from_path(template_path)?;

        if template_config.files.len() > 1 {
            return Err(anyhow!(
                "Template file must contain a single output file, found {}",
                template_config.files.len()
            ));
        }

        let template_config = template_config.files.first().unwrap();

        // favor override values but fall back to template
        let rows = current_config.rows.unwrap_or(template_config.rows);
        let missing_reference_behavior = current_config
            .missing_reference_behavior
            .unwrap_or_else(|| template_config.missing_reference_behavior.to_owned());
        let sort_column_names = current_config
            .sort_column_names
            .unwrap_or_else(|| template_config.column_options.sort_column_names.to_owned());
        let key_column_names = if let Some(k) = current_config.key_column_name {
            vec![k]
        } else {
            template_config.column_options.key_column_names.to_owned()
        };
        let include_header = current_config
            .has_header
            .unwrap_or(template_config.has_header);
        let column_delimiter: u8 = if let Some(cd) = current_config.column_delimiter {
            let bytes = cd.as_bytes();
            if bytes.len() > 1 {
                return Err(anyhow!("Could not parse {} as an ASCII character.", cd));
            }
            bytes[0]
        } else {
            template_config.column_delimiter
        };

        // Parse the columns provided in the current file
        let (types, column_names) = if let Some(columns) = current_config.columns {
            let types: Result<Vec<TypeInfo>> =
                columns.iter().map(|c| TypeInfo::new(&c.expr)).collect();
            let column_names = columns.iter().map(|c| c.name.to_owned()).collect();
            (types?, column_names)
        } else {
            (vec![], vec![])
        };

        // We want all existing columns, with overrides, if any
        // Any columns provided in the current config that were not present in the template config
        // are appended to the end of the list.
        let mut new_columns: HashMap<String, &TypeInfo> = HashMap::new();
        let mut seen_column_names = HashSet::new();
        let mut final_columns: Vec<(String, TypeInfo)> = vec![];

        for (index, c) in column_names.iter().enumerate() {
            new_columns.insert(c.to_owned(), &types[index]);
        }

        for (name, type_info) in &template_config.column_options.columns {
            let name = name.to_owned();
            seen_column_names.insert(name.to_owned());
            if let Some(type_info) = new_columns.get(&name) {
                final_columns.push((name, (*type_info).to_owned()));
            } else {
                final_columns.push((name, (*type_info).to_owned()));
            }
        }

        for name in column_names.iter() {
            if !seen_column_names.contains(name) {
                let type_info = new_columns.get(name).unwrap();
                final_columns.push((name.to_owned(), (*type_info).to_owned()));
            }
        }

        let column_options =
            ColumnOptions::new(final_columns, key_column_names, sort_column_names)?;
        Ok(VerboseConfig {
            column_options,
            rows,
            output_file: current_config.output_file,
            missing_reference_behavior,
            has_header: include_header,
            column_delimiter,
        })
    }
}

impl VerboseConfig {
    pub fn get_column_names(&self) -> Vec<&String> {
        self.column_options
            .columns
            .iter()
            .map(|(name, _)| name)
            .collect()
    }

    pub fn get_type_info(&self) -> Vec<&TypeInfo> {
        self.column_options
            .columns
            .iter()
            .map(|(_, type_info)| type_info)
            .collect()
    }
}
