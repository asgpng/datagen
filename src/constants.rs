pub const DATETIME_FORMAT: &str = "%Y-%m-%d %H:%M:%S";
pub const DATE_FORMAT: &str = "%Y-%m-%d";
pub const TIME_FORMAT: &str = "%H:%M:%S";
pub const DEFAULT_COLUMN_DELIMITER: u8 = b'\t';
pub const DEFAULT_ROW_DELIMITER: u8 = b'\n';
