use crate::config::MissingReferenceBehavior;
use crate::context::{RangeDistribution, RangeWeights, ReferenceMap};
use crate::util::build_reference_key_csv;
use anyhow::{anyhow, Context, Result};
use rand::distributions::WeightedIndex;
use std::collections::HashMap;
use std::path::Path;

pub fn get_context(path: &str) -> Result<RangeDistribution> {
    let path = Path::new(&path);
    let mut reader =
        csv::Reader::from_path(path).with_context(|| format!("Error reading file {:?}", path))?;

    let mut ranges: Vec<(f64, f64)> = vec![];
    let mut weights: Vec<f64> = vec![];
    for result in reader.records() {
        let record: csv::StringRecord = result?;
        let min: f64 = record[0].parse()?;
        let max: f64 = record[1].parse()?;
        let weight: f64 = record[2].parse()?;

        ranges.push((min, max));
        weights.push(weight);
    }

    let distribution = WeightedIndex::new(weights)?;

    Ok((ranges, distribution))
}

pub fn get_reference_context(
    path: &str,
    min_index: usize,
    max_index: usize,
    weight_index: usize,
    column_names: Vec<&String>,
    missing_reference_behavior: &MissingReferenceBehavior,
    reference_columns: Vec<(String, usize)>,
    column_delimiter: u8,
) -> Result<(
    Vec<usize>,
    MissingReferenceBehavior,
    ReferenceMap<RangeDistribution>,
)> {
    let path = Path::new(&path);
    let mut reader =
        csv::Reader::from_path(path).with_context(|| format!("Error reading file {:?}", path))?;

    // column indices of reference columns in the weights file
    let reference_column_indices: Vec<usize> = reference_columns.iter().map(|(_, i)| *i).collect();

    let mut distributions: HashMap<String, RangeWeights> = HashMap::new();
    for record in reader.records() {
        let record: csv::StringRecord = record?;
        let key =
            build_reference_key_csv(&reference_column_indices, &record, column_delimiter as char);
        let min: f64 = record[min_index].parse()?;
        let max: f64 = record[max_index].parse()?;
        let weight: f64 = record[weight_index].parse()?;

        match distributions.get_mut(&key) {
            Some((items, weights)) => {
                items.push((min, max));
                weights.push(weight);
            }
            None => {
                distributions.insert(key, (vec![(min, max)], vec![weight]));
            }
        }
    }

    let distributions = distributions
        .into_iter()
        .map(|(k, v)| {
            let (items, weights) = v;
            let distribution = WeightedIndex::new(weights).unwrap();
            (k, (items, distribution))
        })
        .collect();

    // Column indices of reference columns in the intermediate buffer
    let buffer_column_indices: Result<Vec<usize>> = reference_columns
        .iter()
        .map(|(name, _)| {
            if let Some((index, _)) = column_names.iter().enumerate().find(|(_, n)| *n == &name) {
                Ok(index)
            } else {
                Err(anyhow!("Column name {} was not present.", name))
            }
        })
        .collect();

    Ok((
        buffer_column_indices?,
        missing_reference_behavior.to_owned(),
        distributions,
    ))
}
