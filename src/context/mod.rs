mod cdf;
mod set;

use crate::config::{F64Expression, I64Expression, TypeInfo};
use crate::config::{MissingReferenceBehavior, VerboseConfig};
use anyhow::Result;
use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
use rand::distributions::WeightedIndex;
use std::collections::HashMap;

/// Object containing state which can be captured by the data generating closures
#[derive(Debug)]
pub enum ClosureContext {
    Boolean {
        probability: f64,
    },
    Float {
        min: f64,
        max: f64,
    },
    ContinuousDistribution {
        distribution: WeightedIndex<f64>,
        // (min, max)
        ranges: Vec<(f64, f64)>,
    },
    ReferenceContinuousDistribution {
        column_indices: Vec<usize>,
        missing_reference_behavior: MissingReferenceBehavior,
        distributions: HashMap<String, RangeDistribution>,
    },
    Datetime {
        min: NaiveDateTime,
        max: NaiveDateTime,
    },
    Date {
        min: NaiveDate,
        max: NaiveDate,
    },
    Time {
        min: NaiveTime,
        max: NaiveTime,
    },
    Integer {
        min: i64,
        max: i64,
    },
    I64Expression(I64Expression),
    F64Expression(F64Expression),
    Set {
        items: Vec<String>,
        distribution: WeightedIndex<f64>,
    },
    ReferenceSet {
        column_indices: Vec<usize>,
        missing_reference_behavior: MissingReferenceBehavior,
        distributions: ReferenceMap<StringDistribution>,
    },
}

impl ClosureContext {
    pub fn new(type_info: TypeInfo, config: &VerboseConfig) -> Result<ClosureContext> {
        let VerboseConfig {
            missing_reference_behavior,
            ..
        } = config;
        let column_names: Vec<&String> = config.get_column_names();
        let context = match type_info {
            TypeInfo::Boolean { probability } => ClosureContext::Boolean { probability },
            TypeInfo::Float { min, max } => ClosureContext::Float { min, max },
            TypeInfo::Integer { min, max } => ClosureContext::Integer { min, max },
            TypeInfo::Date { min, max } => ClosureContext::Date { min, max },
            TypeInfo::Time { min, max } => ClosureContext::Time { min, max },
            TypeInfo::Datetime { min, max } => ClosureContext::Datetime { min, max },
            TypeInfo::Set {
                path,
                column_index,
                weight_index,
                reference_columns,
            } => {
                if let Some(reference_columns) = reference_columns {
                    let (column_indices, missing_reference_behavior, distributions) =
                        set::get_reference_context(
                            &path,
                            column_index,
                            weight_index,
                            column_names,
                            missing_reference_behavior,
                            reference_columns,
                            config.column_delimiter,
                        )?;
                    ClosureContext::ReferenceSet {
                        column_indices,
                        missing_reference_behavior,
                        distributions,
                    }
                } else {
                    let (items, distribution) =
                        set::get_context(&path, column_index, weight_index)?;
                    ClosureContext::Set {
                        items,
                        distribution,
                    }
                }
            }
            TypeInfo::ContinuousDistribution {
                path,
                min_index,
                max_index,
                weight_index,
                reference_columns,
            } => {
                if let Some(reference_columns) = reference_columns {
                    let (column_indices, missing_reference_behavior, distributions) =
                        cdf::get_reference_context(
                            &path,
                            min_index,
                            max_index,
                            weight_index,
                            column_names,
                            missing_reference_behavior,
                            reference_columns,
                            config.column_delimiter,
                        )?;
                    ClosureContext::ReferenceContinuousDistribution {
                        column_indices,
                        missing_reference_behavior,
                        distributions,
                    }
                } else {
                    let (ranges, distribution) = cdf::get_context(&path)?;
                    ClosureContext::ContinuousDistribution {
                        ranges,
                        distribution,
                    }
                }
            }
            TypeInfo::I64Expression(exp) => ClosureContext::I64Expression(exp),
            TypeInfo::F64Expression(exp) => ClosureContext::F64Expression(exp),
        };

        Ok(context)
    }
}

type StringDistribution = (Vec<String>, WeightedIndex<f64>);
type RangeDistribution = (Vec<(f64, f64)>, WeightedIndex<f64>);
type ReferenceMap<T> = HashMap<String, T>;
type RangeWeights = (Vec<(f64, f64)>, Vec<f64>);
