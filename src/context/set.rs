use crate::config::MissingReferenceBehavior;
use crate::context::{ReferenceMap, StringDistribution};
use crate::util::build_reference_key_csv;
use anyhow::{anyhow, Context, Result};
use rand::distributions::WeightedIndex;
use std::collections::HashMap;
use std::path::Path;

pub fn get_context(
    path: &str,
    column_index: usize,
    weight_index: Option<usize>,
) -> Result<StringDistribution> {
    let path = Path::new(path);
    let mut reader =
        csv::Reader::from_path(path).with_context(|| format!("Error reading file {:?}", path))?;
    let mut items: Vec<String> = vec![];
    let mut weights: Vec<f64> = vec![];

    for result in reader.records() {
        let record: csv::StringRecord = result?;
        let weight = if let Some(index) = weight_index {
            record[index].parse()?
        } else {
            1.0
        };
        if let Some(item) = record.get(column_index) {
            items.push(item.to_owned());
        } else {
            return Err(anyhow!(
                "Record {:?} has no value for column index {}",
                record,
                column_index
            ));
        }

        weights.push(weight);
    }

    let distribution = WeightedIndex::new(weights)?;

    Ok((items, distribution))
}

pub fn get_reference_context(
    path: &str,
    column_index: usize,
    weight_index: Option<usize>,
    column_names: Vec<&String>,
    missing_reference_behavior: &MissingReferenceBehavior,
    reference_columns: Vec<(String, usize)>,
    column_delimiter: u8,
) -> Result<(
    Vec<usize>,
    MissingReferenceBehavior,
    ReferenceMap<StringDistribution>,
)> {
    let path = Path::new(&path);
    let mut reader =
        csv::Reader::from_path(path).with_context(|| format!("Error reading file {:?}", path))?;

    // column indices of reference columns in the weights file
    let reference_column_indices: Vec<usize> = reference_columns.iter().map(|(_, i)| *i).collect();

    let mut distributions: HashMap<String, (Vec<String>, Vec<f64>)> = HashMap::new();
    for record in reader.records() {
        let record: csv::StringRecord = record?;
        let key =
            build_reference_key_csv(&reference_column_indices, &record, column_delimiter as char);
        let item = record[column_index].to_owned();
        let weight = if let Some(index) = weight_index {
            record[index].parse()?
        } else {
            1.0
        };

        match distributions.get_mut(&key) {
            Some((items, weights)) => {
                items.push(item);
                weights.push(weight);
            }
            None => {
                distributions.insert(key, (vec![item], vec![weight]));
            }
        }
    }

    let distributions = distributions
        .into_iter()
        .map(|(k, v)| {
            let (items, weights) = v;
            let distribution = WeightedIndex::new(weights).unwrap();
            (k, (items, distribution))
        })
        .collect();

    // Column indices of reference columns in the intermediate buffer
    let buffer_column_indices: Result<Vec<usize>> = reference_columns
        .iter()
        .map(|(name, _)| {
            if let Some((index, _)) = column_names.iter().enumerate().find(|(_, n)| *n == &name) {
                Ok(index)
            } else {
                Err(anyhow!("Column name {} was not present.", name))
            }
        })
        .collect();

    Ok((
        buffer_column_indices?,
        missing_reference_behavior.to_owned(),
        distributions,
    ))
}
