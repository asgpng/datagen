use crate::gen::WriteColumnClosure;
use anyhow::Result;
use rand::prelude::*;
use rand::seq::IteratorRandom;
use rand::Rng;
use std::collections::HashSet;
use std::thread::{self, JoinHandle};

// Try up to this many times before giving up (which would suggest the range for generating
// distinct values is too small)
const KEY_CREATION_ATTEMPTS_PER_ITERATION: usize = 10;

// If total range is below this threshold, generate all numbers explicitly
const KEY_THRESHOLD_CARDINALITY: i64 = 1_000_000;

// If ratio of desired numbers to available numbers is below this range, generate all numbers
// explicitly.
const KEY_THRESHOLD_RATIO: f64 = 0.5;

/// Construct a closure representing instructions to execute for creating a key column of the
/// specified number of rows. Each closure should:
/// 1. Generate a new random value for the associated column type withing the specified
///    constraints.
/// 2. Write the bytes to the output buffer passed into the closure.
/// 3. Increment the `bytes_written` counter accordingly.
/// 4. If the column is used as a reference source of a `NestedWeightedIndex` column, record the
///    bytes written to a row-level cache, `columns`.
pub fn build_integer_key_closure(min: i64, max: i64, rows: usize) -> Result<WriteColumnClosure> {
    // Unlike normal closure generation, we generate the random values _before_ entering
    // the main closure execution loop. This ensures that we have a distinct set of the
    // desired size up front. An alternative approach would be to generate them on-the-fly,
    // but this would require passing around the HashSet as a closure argument (as it would
    // need to mutate in the course of execution and thus could not be embedded in the
    // closure). Another advantage of doing this work up front is that it allows us to do
    // less work in the event that the int range is too small for the number of distinct
    // values desired.
    let dop = num_cpus::get() as i64;
    let rows = rows as i64;
    let chunk_size = (max - min) / dop;
    let handles: Vec<JoinHandle<Vec<i64>>> = (0..dop)
            .into_iter()
            .map(|i| {
                thread::spawn(move || {
                    // range
                    let local_min = min + chunk_size * i;
                    let local_max = min + chunk_size * (i + 1) - 1;
                    let local_max = if local_max > max { max } else { local_max };

                    // number of elements to generate
                    let mut local_rows = rows / dop;
                    if i == dop - 1 && local_rows * dop < rows {
                        local_rows = rows - local_rows * (dop - 1);
                    }

                    let mut local_rng = thread_rng();

                    // There are two main scenarios:
                    // 1. The range is small enough to fit in memory -> Iterate all possible values and choose from
                    //    among them
                    // 2. The range is too large to easily fit in memory -> Generate values until the desired
                    //    number of distinct values is created.
                    if max - min < KEY_THRESHOLD_CARDINALITY
                        || (max - min) as f64 / rows as f64 > KEY_THRESHOLD_RATIO
                    {
                        (local_min..local_max)
                            .into_iter()
                            .choose_multiple(&mut local_rng, local_rows as usize)
                    }
                    else {
                        let mut distinct_values: HashSet<i64> = HashSet::new();

                        while distinct_values.len() < local_rows as usize {
                            let mut i = 0;
                            loop {
                                let num: i64 = local_rng.gen_range(local_min..local_max);
                                if distinct_values.contains(&num) {
                                    i += 1;
                                    if i == KEY_CREATION_ATTEMPTS_PER_ITERATION {
                                        panic!("Failed to generate unique value after {} attempts within the range ({}, {})",
                                                KEY_CREATION_ATTEMPTS_PER_ITERATION, local_min, local_max);
                                    }
                                } else {
                                    distinct_values.insert(num);
                                    break;
                                }
                            }
                        }

                        distinct_values.into_iter().collect()
                    }
                })
            })
            .collect();

    let mut distinct_values = vec![];
    for h in handles {
        let result = h.join().expect("Failed to join thread");
        distinct_values.extend_from_slice(&result[..]);
    }

    let closure: WriteColumnClosure = Box::new(move |_, buffer, bytes_written, _, row_index| {
        let mem = distinct_values[*row_index];
        let mem = mem.to_string();
        let mem = mem.as_bytes();

        buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(mem);
        *bytes_written += mem.len();
    });

    Ok(closure)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn random_without_repeats() {
        let mut rng = thread_rng();
        let seq = (0..100).into_iter().choose_multiple(&mut rng, 100);
        let mut distinct = HashSet::new();
        for i in seq.iter() {
            distinct.insert(*i);
        }

        assert_eq!(distinct.len(), seq.len());
    }
}
