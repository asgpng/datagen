//! `data_gen` is the core library for `data_gen`, an executable for high-performance generation of
//! fake data. This library defines an interface for accessing the internal generation logic
//! through a library form rather than a binary.

#[macro_use]
extern crate lalrpop_util;

pub mod config;
mod constants;
mod context;
mod gen;
mod util;

pub use gen::process;
