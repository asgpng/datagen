mod command;

use clap::clap_app;
use data_gen::config::{ConciseConfig, ParseableConfig};
use env_logger::{Builder, Env};
use log::*;

fn main() {
    Builder::from_env(Env::default().default_filter_or("info")).init();
    let matches = clap_app!(datagen =>
        (version: "0.1.1")
        (author: "Adam Gallagher <asgpng@gmail.com>")
        (about: "High-performance pseudo-random data generation")
        (@subcommand check =>
            (about: "Verify input arguments")
            (@arg layout_file: --layout_file +takes_value +required
                "File containing fake data layout in TOML")
        )
        (@subcommand output =>
            (about: "Generate data")
            (@arg layout_file: --layout_file +takes_value +required
                "File containing fake data layout in TOML")
        )
    )
    .get_matches();

    match matches.subcommand_name() {
        Some("check") => {
            match command::handle_layout_check_command(matches.subcommand_matches("check").unwrap())
            {
                Ok(config) => {
                    info!("Successfully parsed config: {:?}.", config);
                }
                Err(e) => {
                    error!("Failed to parse file: {:?}", e);
                }
            }
        }
        Some("output") => {
            match command::handle_output_command(matches.subcommand_matches("output").unwrap()) {
                Ok(()) => {}
                Err(e) => {
                    error!("Failed: {:?}", e);
                }
            }
        }
        _ => {
            error!("Failed to parse command.");
        }
    }
}

fn _test_toml() {
    let config = ParseableConfig::Multiple {
        files: vec![
            ParseableConfig::Concise(ConciseConfig {
                rows: 0,
                output_file: "".to_string(),
                columns: vec![],
                key_column_name: None,
                missing_reference_behavior: None,
                sort_column_names: None,
                has_header: None,
                column_delimiter: None,
            }),
            ParseableConfig::Concise(ConciseConfig {
                rows: 0,
                output_file: "".to_string(),
                columns: vec![],
                key_column_name: None,
                missing_reference_behavior: None,
                sort_column_names: None,
                has_header: None,
                column_delimiter: None,
            }),
        ],
    };
    let t = toml::to_string(&config);
    println!("{:?}", t);
}
